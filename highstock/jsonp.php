<?php
include "config.php";
$callback = (string)$_GET['callback'];
if (!$callback) $callback = 'callback';

$query = query("
select 	current_datetime  as datetime,voltage
		from amp3b");

while($row=mysqli_fetch_array($query)){
	$data[]=array($row['datetime']*1000 ,(float)$row['voltage']);
}
$json =json_encode($data, JSON_NUMERIC_CHECK);
header('Content-Type: text/javascript');
echo "$callback($json);";
?>