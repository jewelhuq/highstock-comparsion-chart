<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />

        <title></title>
		<link href="assets/css/xcharts.min.css" rel="stylesheet">
		<link href="assets/css/style.css" rel="stylesheet">

		<!-- Include bootstrap css -->
		<link href="assets/css/daterangepicker.css" rel="stylesheet">
		<link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.2.2/css/bootstrap.min.css" rel="stylesheet" />

    </head>
    <body>
		<div id="content">
			
			<form class="form-horizontal">
			  <fieldset>
		        <div class="input-prepend">
		          <span class="add-on"><i class="icon-calendar"></i></span><input type="text" name="range" id="range" />
		        </div>
			  </fieldset>
			</form>
			
			<div id="placeholder">
				<figure id="chart"></figure>
			</div>

		</div>

		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/d3/2.10.0/d3.v2.js"></script>
		<script src="assets/js/xcharts.min.js"></script>

		
		<script src="assets/js/sugar.min.js"></script>
		<script src="assets/js/daterangepicker.js"></script>

		<!-- Our main script file -->
		<script src="assets/js/script.js"></script>

		
        
    </body>
</html>

<?php

require_once('setup.php');
try{
	
	for($i = 0; $i < 30; $i++){
	
		$rand  = rand(9, 100);
		$sales = $sales = R::dispense('amp3b');
		$sales->current_date     = date('Y-m-d', time() - $i*24*60*60);
		$sales->current_datetime =time();
		$sales->voltage = $rand;
		R::store($sales);
	
	}

}
catch(PDOException $e){
echo "";
}



?>
