<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//code.highcharts.com/highcharts.js"></script>

<div id="chart"></div>

<input type="button" value="Show HighChart" onclick="InitHighChart();" />
<script>
function InitHighChart()
{
	$("#chart").html("Wait, Loading graph...");
	
	var options = {
		chart: {
			renderTo: 'chart',type: 'spline',
		},
		credits: {
			enabled: false
		},
		title: {
			text: 'Impression/Click Overview',
			x: -20
		},
		xAxis: {
			categories: [{}]
		},
		tooltip: {
            formatter: function() {
                var s = '<b>'+ this.x +'</b>';
                
                $.each(this.points, function(i, point) {
                    s += '<br/>'+point.series.name+': '+point.y;
                });
                
                return s;
            },
            shared: true
        },
		series: [{},{},{}]
	};
	
	$.ajax({
		url: "json.php",
		data: 'show=impression',
		type:'post',
		dataType: "json",
		success: function(data){
			options.xAxis.categories = data.categories;
			options.series[0].name = 'Impression';
			options.series[0].data = data.impression;
			options.series[1].name = 'Friends';
			options.series[1].data = data.friends;
			options.series[2].name = 'Clicks';
			options.series[2].data = data.clicks;
			
			
			
			
			var chart = new Highcharts.Chart(options);			
		}
	});
	
}

</script>